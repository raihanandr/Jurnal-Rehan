from django.urls import path

from . import views

app_name = 'homeku'

urlpatterns = [
    path('', views.story1, name='index'),
]