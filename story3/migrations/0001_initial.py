# Generated by Django 3.1.1 on 2020-10-14 10:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Matkul', models.CharField(max_length=120)),
                ('Dosen', models.CharField(max_length=120)),
                ('JumlahSKS', models.DecimalField(decimal_places=0, max_digits=100)),
                ('Deskripsi', models.TextField()),
            ],
        ),
    ]
