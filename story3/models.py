from django.db import models

# Create your models here.
# - Nama Mata Kuliah
# - Dosen Pengajar
# - Jumlah SKS (number integer)
# - Deskripsi

class Jadwal(models.Model) :
    Matkul = models.CharField(max_length=120)
    Dosen = models.CharField(max_length=120)
    sks     = (('1','1'),
               ('2','2'),
               ('3','3'),
               ('4','4'),
               ('5','5'),
               ('6','6'),
               ('7','7'),)
    JumlahSKS = models.CharField(max_length = 20, choices = sks, default = '1',)
    Deskripsi = models.TextField(blank=False, null=False)
    ruangan = models.CharField(max_length=120, default="A204")
    CHOICES = (('Gasal 2019/2020','Gasal 2019/2020'),
               ('Genap 2018/2019','Genap 2018/2019'),
               ('Gasal 2018/2019','Gasal 2018/2019'),
               ('Genap 2017/2018','Genap 2017/2018'),
               ('Gasal 2018/2018','Gasal 2018/2018'),
               ('Genap 2016/2017','Genap 2016/2017'),
               ('Gasal 2016/2017','Gasal 2016/2017'),)
    
    semestertahun = models.CharField(max_length = 20, choices = CHOICES, default = '1',)
    
    



    
