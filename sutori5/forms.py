from django.forms import ModelForm
from .models import Jadwal
from django import forms

class Jadwalku(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = [
            'Matkul',
            'Dosen',
            'JumlahSKS',
            'Deskripsi',
            'ruangan',
            'semestertahun',
        ]