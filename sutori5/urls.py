from django.urls import path

from . import views

app_name = 'sutori5'

urlpatterns = [
    path('project/', views.comingsoon, name='comingsoon'),
    path('delete/<int:idinput>', views.delete, name='delete'),
    path('detail/<int:id_detail>', views.detail, name='detail'),
]
